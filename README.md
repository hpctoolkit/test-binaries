## ~~HPCToolkit Test Binaries~~

~~Sample binaries used for testing HPCToolkit software, specifically `hpcstruct`. None of these are expected to actually
be run in practice, but they serve as good inputs for binary analysis.~~

The contents of this repository have been moved into the [main HPCToolkit repository](https://gitlab.com/hpctoolkit/hpctoolkit).
See [hpctoolkit/hpctoolkit!961](https://gitlab.com/hpctoolkit/hpctoolkit/-/merge_requests/961) for the rationale behind this shift.
As such, this repo has been archived for reference and historical purposes.
