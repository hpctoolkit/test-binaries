default_language_version:
  python: python3

# NB: The hooks in this file are listed in a particular order to reduce the number of times
# pre-commit has to run before getting an all-clear.

repos:
# ------------------------------------------------------------------------------------
#  Formatting hooks:  Hooks that alter the syntax but not semantics of files
# ------------------------------------------------------------------------------------

- repo: https://github.com/pre-commit/pre-commit-hooks
  rev: v4.4.0
  hooks:
  # The contents needs to be a sorted list
  - id: file-contents-sorter
    files: contents
  # Remove trailing whitespace
  - id: trailing-whitespace
  # All files must end in a single newline (or be perfectly empty)
  - id: end-of-file-fixer
  # Remove the UTF8 BOM from the start of any files
  - id: fix-byte-order-marker
  # Ensure files have consistent endings. (This operates in the worktree, Git also normalizes the index)
  - id: mixed-line-ending


# ------------------------------------------------------------------------------------
#  Linting hooks:  Hooks that do not alter files but checks that they satisfy various conditions
# ------------------------------------------------------------------------------------

- repo: meta
  hooks:
  # Check that hooks listed actually do something
  - id: check-hooks-apply
  # Check that any excludes do indeed exclude something
  - id: check-useless-excludes

- repo: https://github.com/pre-commit/pre-commit-hooks
  rev: v4.4.0
  hooks:
  # Ensure all executable scripts have a shebang
  - id: check-executables-have-shebangs
  # Ensure large files (>=500KB) are never added outside of LFS
  - id: check-added-large-files
  # Warn if symlinks are ever accidentally destroyed
  - id: destroyed-symlinks
  # Ensure conflict markers are never committed anywhere
  - id: check-merge-conflict
  # Ensure files do not differ only in case (problematic on some filesystems)
  - id: check-case-conflict
  # Reminder to always work in a branch separate from main
  - id: no-commit-to-branch
    args: [--branch, main]

- repo: https://github.com/shellcheck-py/shellcheck-py
  rev: v0.9.0.2
  hooks:
  # Find common errors in shell scripts using shellcheck
  - id: shellcheck

- repo: https://github.com/python-jsonschema/check-jsonschema
  rev: '0.20.0'
  hooks:
  # Validate the GitLab CI scripts against the schema. Doesn't catch everything but helps
  - id: check-gitlab-ci

- repo: https://github.com/codespell-project/codespell
  rev: v2.2.2
  hooks:
  # Identify common spelling mistakes in code and comments
  - id: codespell
    args: ['--config', '.codespellrc']

- repo: https://github.com/Yelp/detect-secrets
  rev: v1.4.0
  hooks:
  # Scan for secrets that should never appear in the repo itself
  - id: detect-secrets
